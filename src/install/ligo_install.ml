let ligo_prefix = match Sys.getenv_opt "LIGO_PREFIX" with
  | None -> "/usr/local/bin"
  | Some s -> s

let installed () =
  let stdout = Filename.temp_file "ligo_install_version" "" in
  Sys.command @@ Filename.quote_command ~stdout "ligo" ["--version"] = 0

let static_install () =
  let r = Sys.command @@ Format.sprintf
      "wget -q https://ligolang.org/bin/linux/ligo -O ligo && \
       chmod +x ligo && mv ligo %s" ligo_prefix in
  if r <> 0 then Format.printf "Ligo install failed@."

let docker_install () =
  let oc = open_out_gen [ Open_wronly; Open_text; Open_creat; Open_trunc ] 0o555 @@
    Filename.concat ligo_prefix "ligo" in
  output_string oc "#!/bin/sh\ndocker run --rm -v $PWD:$PWD -w $PWD ligolang/ligo:0.27.0 $@";
  close_out oc

let () =
  if installed () then ()
  else
    let docker = Array.length Sys.argv > 1 && Sys.argv.(1) = "--docker" in
    let ic = Unix.open_process_in "uname" in
    let uname = String.trim @@ input_line ic in
    close_in ic;
    if uname = "Linux" && not docker then static_install ()
    else docker_install ()
