open Std
type michelson_program = string
type ligo_program = string
type test_exec_error =
  | Rejected of (michelson_program * address)
  | Other
type test_exec_result =
  | Success
  | Fail of test_exec_error
type ('p, 's) typed_address = string
type mutation = string

let to_contract _ = assert false
let to_entrypoint _ _ = assert false
let originate_from_file _ _ _ _ _ = ("", "", 0)
let originate _ _ _ = "", "", 0
let set_now _ = ()
let set_source _ = ()
let set_baker _ = ()
let transfer _ _ _ = Success
let transfer_exn _ _ _ = ()
let transfer_to_contract _ _ _ = Success
let transfer_to_contract_exn _ _ _ = ()
let get_storage_of_address _ = ""
let get_storage _ = assert false
let get_balance _ = 0
let michelson_equal _ _ = false
let log _ = assert false
let reset_state _ _ = ()
let nth_bootstrap_account _ = ""
let compile_expression _ _ = ""
let compile_expression_subst _ _ _ = ""
let compile_value _ = ""
let eval _ = ""
let run _ _ = ""
let mutate_count _ = 0
let mutate_expression _ _ = 0
let mutate_value _ _ = None
let mutation_test _ _ = None
let mutation_test_all _ _ = assert false
let create_chest _bytes _n = (), ""
let create_chest_key _chest _n = ""
