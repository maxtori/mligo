open Std

type ('param, 'storage, 'res) view = {
  vstorage: 'storage;
  view: ('param * 'storage) -> 'res
}

type context = {
  source: address;
  mutable sender: address;
  mutable self: address;
  level: int;
  time: timestamp;
  mutable amount: tez;
  chain_id: string;
  mutable contracts: (string, string) contract Stdlib.List.t;
  mutable views: ((address * string) * (string, string, string) view) list;
}

val balance : context option -> tez
val now : context option -> timestamp
val amount : context option -> tez
val sender : context option -> address
val address : _ contract -> address
val self_address : context option -> address
val self : context option -> string -> _ contract
val source : context option -> address
val chain_id : context option -> chain_id
val transaction : context option -> 'parameter -> tez -> ('parameter, _) contract -> operation
val set_delegate : context option -> key_hash option -> operation
val get_contract_opt : context option -> address -> _ contract option
val get_entrypoint_opt : context option -> string -> address -> _ contract option
val level : context option -> nat
val implicit_account : context option -> key_hash -> (unit, unit) contract
val open_chest : context option -> chest_key -> chest -> nat -> chest_opening_result
val call_view : context option -> string -> 'arg -> address -> 'ret option

(* to use to build context *)
val mk_implicit_account : ?balance:nat -> ?delegate:key_hash -> address -> (string, string) contract
val mk_contract : ?balance:nat -> ?delegate:key_hash ->
  storage:'storage -> entrypoint:string -> address:address ->
  (('param * 'storage) -> operation list * 'storage) ->
  (string, string) contract
val mk_view : storage:'storage -> entrypoint:string -> address:address ->
  (('param * 'storage) -> 'res) -> ((string * string) * (string, string, string) view)
